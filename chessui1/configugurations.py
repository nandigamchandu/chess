"""Configures for chess board."""

NUMBER_OF_ROWS = 8
NUMBER_OF_COLUMNS = 8
DIMENSION_OF_EACH_SQUARE = 64
BOARD_COLOR_1 = "#DDB88C"
BOARD_COLOR_2 = "#A66D4F"
X_AXIS_LABELS = ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H')
Y_AXIS_LABELS = (1, 2, 3, 4, 5, 6, 7, 8)
START_PIECES_POSITION = ({
 "a8": "r", "b8": "n", "c8": "b",  "d8": "q", "e8": "k", 'f8': "b",
 "g8": "n", "h8": "r",
 "a7": "p", "b7": "p", "c7": "p", "d7": "p", "e7": "p", 'f7': "p",
 "g7": "p", "h7": "p",
 "a2": "P", "b2": "P", "c2": "P", "d2": "P", "e2": "P", 'f2': "P",
 "g2": "P", "h2": "P",
 "a1": "R", "b1": "N", "c1": "B", "d1": "Q", "e1": "K", 'f1': "B",
 "g1": "N", "h1": "R",
})
