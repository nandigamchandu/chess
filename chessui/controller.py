"""Controller of chess."""


class Controller:
    """Control moves."""

    def __init__(self):
        """Contruct for controller class."""
        self.start_game()

    def start_game(self):
        """Start Game."""
        self.position = ({
         "a8": "r", "b8": "n", "c8": "b",  "d8": "q", "e8": "k", 'f8': "b",
         "g8": "n", "h8": "r",
         "a7": "p", "b7": "p", "c7": "p", "d7": "p", "e7": "p", 'f7': "p",
         "g7": "p", "h7": "p",
         "a2": "P", "b2": "P", "c2": "P", "d2": "P", "e2": "P", 'f2': "P",
         "g2": "P", "h2": "P",
         "a1": "R", "b1": "N", "c1": "B", "d1": "Q", "e1": "K", 'f1': "B",
         "g1": "N", "h1": "R",
        })

    def updatePos(self, input):
        """Update position."""
        lst = []
        f = input[:2]
        t = input[2:]
        for key, value in self.position.items():
            if f == key:
                key = t
            lst.append((key, value))
        self.position = dict(lst)
        return self.position


class Piece:
    """Piece class implement."""

    def __init__(self, color, name, row, column):
        """Contruct."""
        self.color = color
        self.name = name
        self.row = row
        self.column = column
        self.row_init = row
        self.col_init = column

    def update_position(self, row, column):
        """Update position."""
        self.row_init = self.row
        self.col_init = self.column
        self.row = row
        self.column = column
        return self

    def get_position(self):
        """Get present position."""
        return self.row, self.column, self.row_init, self.col_init


class King(Piece):
    """Implement king."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check current move is valid or not."""
        r_p, c_p, r_i, c_i = self.get_position()
        c_p, col = ord(c_p), ord(col)
        rule = row in [r_p-1, r_p, r_p+1] and col in [c_p-1, c_p, c_p+1]

        x = col+1 if col > c_p else col-2
        is_piece = ph.placeholder.get((row, chr(x)), '  ')

        # castling
        rule4 = False
        # checking is king moved or not? and
        # is rook piece present at a or h postions
        if all([row == r_p, r_p == r_i, chr(c_p) == c_i, is_piece != '  ']):
            rule1 = is_piece.name in ['R', 'r']
            rp, cp, ri, ci = is_piece.get_position()
            rule2 = rp == ri and cp == ci
            rule3 = all([rule1, rule2])
            i = 1 if self.color is 'white' else 8
            # is rook moved from initial position or not?
            if rule3 and col > c_p:
                rule4 = (all(map(lambda x: ph.get_piece(i, chr(c_p+x)) == '  ',
                                 range(ord(cp)+1, ord('h')))))
            elif rule3 and col < c_p:
                rule4 = (all(map(lambda x: ph.get_piece(i, chr(c_p-x)) == '  ',
                                 range(ord('a')-1, ord(cp)))))
        return rule or rule4

    def is_check(self, ph):
        """Is king in check position."""
        r_p, c_p, r_i, c_i = self.get_position()
        for key, value in ph.placeholder.items():
            if value != '  ':
                if value.color != self.color and value.is_can(r_p, c_p, ph):
                    return True
        return False


class Queen(Piece):
    """Queen class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check move is valide or not."""
        r_p, c_p, r_i, c_i = self.get_position()
        rule1 = Rook.is_can(self, row, col, ph)
        rule2 = Bishop.is_can(self, row, col, ph)
        return rule1 or rule2


class Rook(Piece):
    """Rook class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check present move is right or not."""
        rule1 = False
        r_p, c_p, r_i, c_i = self.get_position()
        c_p, col = ord(c_p), ord(col)
        if r_p < row and c_p == col:
            rule1 = all(map(lambda x: ph.get_piece(x, chr(c_p)) == '  ',
                            range(r_p+1, row)))
        elif r_p > row and c_p == col:
            rule1 = all(map(lambda x: ph.get_piece(x, chr(c_p)) == '  ',
                            range(row+1, r_p)))
        elif c_p < col and row == r_p:
            rule1 = all(map(lambda x: ph.get_piece(r_p, chr(x)) == '  ',
                            range(c_p+1, col)))
        elif c_p > col and row == r_p:
            rule1 = all(map(lambda x: ph.get_piece(r_p, chr(x)) == '  ',
                            range(col+1, c_p)))

        return rule1


class Knight(Piece):
    """Knight class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check move is valide or not."""
        r_p, c_p, r_i, c_i = self.get_position()
        c_p = ord(c_p)
        col = ord(col)
        chances = [(r_p+2, c_p+1), (r_p+2, c_p-1), (r_p-2, c_p+1),
                   (r_p-2, c_p-1), (r_p+1, c_p+2), (r_p+1, c_p-2),
                   (r_p-1, c_p+2), (r_p-1, c_p-2)]
        return (row, col) in chances


class Bishop(Piece):
    """Bishop class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check move is valide or not."""
        rule1 = False
        r_p, c_p, r_i, c_i = self.get_position()
        c_p, col = ord(c_p), ord(col)
        if abs(r_p-row) == abs(c_p-col):
            if r_p < row and c_p < col:
                rule1 = (all(map(
                        lambda x: ph.get_piece(r_p+x, chr(c_p+x)) == '  ',
                        range(1, row-r_p))))
            elif r_p < row and c_p > col:
                rule1 = (all(map(
                        lambda x: ph.get_piece(r_p+x, chr(c_p-x)) == '  ',
                        range(1, row-r_p))))
            elif r_p > row and c_p < col:
                rule1 = (all(map(
                        lambda x: ph.get_piece(r_p-x, chr(c_p+x)) == '  ',
                        range(1, r_p-row))))
            elif r_p > row and c_p > col:
                rule1 = (all(map(
                        lambda x: ph.get_piece(r_p-x, chr(c_p-x)) == '  ',
                        range(1, r_p-row))))
        return rule1


class Pawn(Piece):
    """Pawn class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check present move is right or not."""
        r_p, c_p, r_i, c_i = self.get_position()
        piece = ph.get_piece(row, col)
        rule3 = ph.get_piece(row, col) == '  '
        rule4 = False
        rule5 = False

        # condition for one jump and two jumps
        if self.color is 'white':
            rule1 = r_p is 2 and r_p+2 is row and c_p is col
            rule2 = r_p+1 is row and c_p is col
        else:
            rule1 = r_p is 7 and r_p-2 is row and c_p is col
            rule2 = r_p-1 is row and c_p is col

        # condition for cross jump
        if piece != '  ' and self.color is 'white':
            rule4 = ord(col) in [ord(c_p)+1, ord(c_p)-1] and r_p+1 is row
        elif piece != '  ' and self.color is 'black':
            rule4 = ord(col) in [ord(c_p)+1, ord(c_p)-1] and r_p-1 is row

        # en passant condition
        last_move = ph.lastmoves[-1]
        diff = abs(last_move[0].row - last_move[0].row_init)
        rule5 = all([last_move[0].name in ['p', 'P'],
                     diff == 2, rule3, last_move[2] == col])
        return any([(rule1 or rule2) and rule3, rule4, rule5])
