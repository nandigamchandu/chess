"""Data about positions."""
from controller import King, Queen, Rook, Knight, Bishop, Pawn


class PlaceHolder:
    """Implement place hold class."""

    def __init__(self):
        """Construct for placeholder."""
        self.placeholder = {}
        self.lastmoves = []

    def initial_placehold(self):
        """Create initial positions."""
        col = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        pawn_row = {'white': [2, 'P'], 'black': [7, 'p']}
        pawn = ([Pawn(key, value[1], value[0], j)
                for key, value in pawn_row.items() for j in col])
        white_king = King('white', 'K', 1, 'e')
        black_king = King('black', 'k', 8, 'e')
        x = [white_king, Queen('white', 'Q', 1, 'd'),
             Rook('white', 'R', 1, 'a'), Rook('white', 'R', 1, 'h'),
             Knight('white', 'N', 1, 'b'), Knight('white', 'N', 1, 'g'),
             Bishop('white', 'B', 1, 'c'), Bishop('white', 'B', 1, 'f'),
             black_king, Queen('black', 'q', 8, 'd'),
             Rook('black', 'r', 8, 'a'), Rook('black', 'r', 8, 'h'),
             Knight('black', 'n', 8, 'b'), Knight('black', 'n', 8, 'g'),
             Bishop('black', 'b', 8, 'c'), Bishop('black', 'b', 8, 'f')] + pawn
        self.placeholder = {(i.row, i.column): i for i in x}
        self.lastmoves.append((x[0], 1, 'e', '  '))
        return self.placeholder, white_king, black_king

    def update_place_holder(self, piece):
        """Update new position."""
        prom = {'1': (Queen, 'Queen', 'Q'), '2': (Knight, 'Knight', 'N'),
                '3': (Rook, 'Rook', 'R'), '4': (Bishop, 'Bishop', 'B')}
        # place holder updating for normal move
        row, column, rowi, coli = piece.get_position()
        capture = self.placeholder.get((row, column), '  ')
        self.placeholder[(row, column)] = piece
        self.placeholder[(rowi, coli)] = '  '

        # place holder updating for prawn promotion
        if piece.name in ['P', 'p'] and row in [1, 8]:
            print('Promotion to')
            for key, value in prom.items():
                print(key, value[1])
            opt = input('choose your option: ')
            piece_c = prom[opt][0]
            name_c = prom[opt][2]
            name_c = name_c if piece.color is 'white' else name_c.lower()
            self.placeholder[(row, column)] = (piece_c('white', name_c,
                                                       row, column))

        col = ord(column)
        c_i = ord(coli)
        # place holder update for castling
        diff = abs(col - c_i)
        if piece.name in ['K', 'k'] and diff == 2:
            x = col+1 if col > c_i else col-2
            rook = self.placeholder.get((row, chr(x)), '  ')
            rp, cp, ri, ci = rook.get_position()
            is_check = piece.is_check(self)
            if not is_check:
                c_c = col - 1 if col > c_i else col + 1
                self.placeholder[(row, chr(c_c))] = (rook
                                                     .update_position(
                                                              row, chr(c_c)))
                self.placeholder[(row, cp)] = '  '

        # place holder update for en passant
        if all([piece.name in ['p', 'P'], abs(row-rowi) == 1,
                abs(col - c_i) == 1, capture == '  ']):
            r_c = row - 1 if piece.name is 'P' else row + 1
            capture = self.placeholder[(r_c, column)]
            self.placeholder[(r_c, column)] = '  '
        self.lastmoves.append((piece, row, column, capture, rowi, coli))

    def get_piece(self, row, column):
        """Return peace at that position."""
        return self.placeholder.get((row, column), '  ')
