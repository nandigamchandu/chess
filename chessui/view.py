"""Chess ui."""

import tkinter as tk
import configugurations as cfg
import controller as ctl
import chess

# colors
DARK_SQUARE_COLOR = "#DDB88C"
LIGHT_SQUARE_COLOR = "#A66D4F"
controller = ctl.Controller()


class ChessBoard(tk.Frame):
    """Create board."""

    def __init__(self, parent, squareWidth=64, squareHeight=64):
        """Constructor."""
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.chessState = None
        self.input = tk.StringVar()
        self.squareWidth = squareWidth
        self.squareHeight = squareHeight
        self.width = 8*squareWidth
        self.height = 8*squareHeight
        self.bottom_frame = tk.Frame(self.parent, height=64)
        self.status1 = tk.StringVar()
        self.status = ' '
        self.info_label = tk.Label(self.bottom_frame,
                                   text=self.status, fg="black")
        self.pieceToPhoto = {}
        self.textEntry = None
        self.chess = chess.Chess()
        self.status1.set(self.chess.get())
        self.board()
        self.loadImages()
        self.inputStatus()

    def board(self):
        """Side labels, board, buttom labels."""
        self.chessboard = tk.Frame(self)
        self.left = tk.Frame(self.chessboard)
        self.board = tk.Frame(self.chessboard)
        self.square = tk.Frame(self.board)
        self.bottom = tk.Frame(self.board)
        self.status = tk.Frame(self)

        self.canvas = tk.Canvas(self.square, width=self.width,
                                height=self.height)
        self.canvas.bind("<Button-1>", self.on_square_clicked)
        self.boardPrint()
        self.square.pack(side='top')
        self.left_frames = [None] * 9
        i = 0
        for label in range(8, 0, -1):
            self.left_frames[i] = tk.Frame(self.left, bd=4)
            (tk.Label(self.left_frames[i], text=label, font=("Times", 16))
               .pack(anchor='center'))
            self.left_frames[i].pack(pady=16)
            i += 1
        self.left_frames[8] = tk.Frame(self.left, bd=2).pack(pady=20)
        self.left.pack(side='left')

        self.bottom_frames = [None] * 8
        i = 0
        for label in 'abcdefgh':
            self.bottom_frames[i] = tk.Frame(self.bottom, bd=5)
            (tk.Label(self.bottom_frames[i], text=label,
                      font=("Times", 16))
               .pack(anchor='center'))
            self.bottom_frames[i].pack(side='left', padx=20)
            i += 1
        self.bottom.pack(side='bottom')
        self.board.pack(side='right')
        self.chessboard.pack(side='top')

    def inputStatus(self):
        """Input and status."""
        (tk.Label(self.status, textvariable=self.status1, width=30,
                  font=("Helvetica", 15))
           .grid(row=0, column=0, padx=1, pady=5,
                 columnspan=40, sticky='e'))
        (tk.Label(self.status, text='Enter Input', font=("Time", 15))
           .grid(row=0, column=60, padx=5))
        self.textEntry = tk.Entry(self.status, width=8,
                                  textvariable=self.input)
        self.textEntry.grid(row=0, column=61, padx=2, pady=2, columnspan=64)
        self.textEntry.focus_set()
        self.textEntry.bind("<Return>", self.getInput)
        self.status.pack(side='left')

    def getInput(self, event):
        """Refresh."""
        input = self.input.get()
        if input[:10] == "restart":
            self.status1.set('Game restarted')
            self.chess.start_game()
            controller.start_game()
            self.clear()
            self.textEntry.delete(0, 'end')
            return
        can = self.chess.main(input)
        if can:
            x = controller.updatePos(input)
            self.status1.set(self.chess.get())
            self.movePiece(x)
            self.textEntry.delete(0, 'end')
        else:
            self.status1.set('Invalid Move')
            self.textEntry.delete(0, 'end')

    def clear(self):
        """Reset board."""
        self.canvas.delete('piece')
        self.refreshCanvasFromState()

    def movePiece(self, positions):
        """Move piece on board."""
        self.canvas.delete('piece')
        for rname in '12345678':
            for fname in 'abcdefgh':
                sname = fname + rname
                piece = positions.get(sname, ' ')
                if piece != ' ':
                        self.placePieces(sname, piece)

    def boardPrint(self):
        """Print board."""
        for ridx, rname in enumerate(list('87654321')):
            for fidx, fname in enumerate(list('abcdefgh')):
                color = ([LIGHT_SQUARE_COLOR,
                          DARK_SQUARE_COLOR][(ridx-fidx) % 2])
                shade = ['dark', 'light'][(ridx-fidx) % 2]
                tags = [fname+rname, shade, 'square']
                (self.canvas.create_rectangle(
                                fidx*self.squareWidth, ridx*self.squareHeight,
                                fidx*self.squareWidth+self.squareWidth,
                                ridx*self.squareHeight+self.squareHeight,
                                outline=color, fill=color, tag=tags))
        self.canvas.grid(row=0, column=0)

    def loadImages(self):
        """Load pieace images."""
        for piece in 'pnbrqkPNBRQK':
            self.pieceToPhoto[piece] = (tk.PhotoImage(
                                               file='./images/%s.png' % piece))

    def on_square_clicked(self, event):
        """Click on square."""
        tag = self.get_clicked_row_column(event)
        print(tag)

    def get_clicked_row_column(self, event):
        """Click event."""
        col_size = row_size = 64
        clicked_column = 1 + (event.x // col_size)
        clicked_row = (event.y // row_size)
        tag = self.canvas.gettags(clicked_row * 8 + clicked_column)
        return tag

    def placePieces(self, square, piece):
        """Place pieces."""
        if piece == ' ':
            return

        item = self.canvas.find_withtag(square)
        coords = self.canvas.coords(item)

        photo = self.pieceToPhoto[piece]
        self.canvas.create_image(coords[0], coords[1], image=photo,
                                 state=tk.NORMAL, anchor='nw',
                                 tag='piece')

    def refreshCanvasFromState(self):
        """Set position."""
        for rname in '12345678':
            for fname in 'abcdefgh':
                sname = fname + rname
                piece = cfg.START_PIECES_POSITION.get(sname, ' ')
                if piece != ' ':
                    self.placePieces(sname, piece)


root = tk.Tk()
root.title('Chess Game')
cbt = ChessBoard(root)
cbt.refreshCanvasFromState()
cbt.pack()
root.mainloop()
