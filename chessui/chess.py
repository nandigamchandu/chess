"""To handle chess errors."""
from model import PlaceHolder


class Chess:
    """Status reporter."""

    def __init__(self):
        """Constructor."""
        self.status = None
        self.start_game()

    def start_game(self):
        """Start game."""
        self.piece_places = PlaceHolder()
        _, self.white_king, self.black_king = (self.piece_places
                                                   .initial_placehold())
        self.curr_move = 'white'
        self.set('Move {}'.format(self.curr_move))

    def set(self, inp):
        """Set status."""
        self.status = inp

    def get(self):
        """Get status."""
        return self.status

    def check_input(self, ph, color, inp):
        """Check input."""
        col = list('abcdefgh')
        row = [1, 2, 3, 4, 5, 6, 7, 8]
        pieces = ph.placeholder
        if len(inp) == 4 and inp[1].isdigit() and inp[3].isdigit():
            c_i, r_i, c_f, r_f = inp[0], int(inp[1]), inp[2], int(inp[3])
            inp1 = all([c_i in col, r_i in row, c_f in col, r_f in row])
            if inp1 and pieces.get((r_i, c_i), '  ') != '  ':
                return r_i, c_i, r_f, c_f
            else:
                self.set('Invalid Input')
        else:
            self.set("Invalid Input")
        return 0, 0, 0, 0

    def main(self, input):
        """Main."""
        row_i, col_i, row_f, col_f = self.check_input(self.piece_places,
                                                      self.curr_move, input)
        if row_i == 0:
            return False
        # self.set('move {}'.format(self.curr_move))
        move_piece = self.piece_places.get_piece(row_i, col_i)
        piece = self.piece_places.get_piece(row_f, col_f)
        can_not_move = not move_piece.is_can(row_f, col_f, self.piece_places)
        color_match = False
        # check killing king or not
        if piece != '  ':
            color_match = piece.color == move_piece.color
        # check which color is moving, which piece is killing
        if any([self.curr_move != move_piece.color,
                can_not_move, color_match]):
            self.set('Invalid Move')
            return False
        else:
            self.curr_move = 'black' if self.curr_move == 'white' else 'white'
        self.set('Move {}'.format(self.curr_move))
        # updating piece and board
        move_piece.update_position(row_f, col_f)
        self.piece_places.update_place_holder(move_piece)
        return True
