"""Chess implement."""


class Piece:
    """Piece class implement."""

    def __init__(self, color, name, row, column):
        """Contruct."""
        self.color = color
        self.name = name
        self.row = row
        self.column = column
        self.row_init = row
        self.col_init = column

    def update_position(self, row, column):
        """Update position."""
        self.row_init = self.row
        self.col_init = self.column
        self.row = row
        self.column = column
        return self

    def get_position(self):
        """Get present position."""
        return self.row, self.column, self.row_init, self.col_init


class PlaceHolder:
    """Implement place hold class."""

    def __init__(self):
        """Construct for placeholder."""
        self.placeholder = {}
        self.lastmoves = []

    def initial_placehold(self):
        """Create initial positions."""
        col = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        pawn_row = {'white': [2, 'P'], 'black': [7, 'p']}
        pawn = ([Pawn(key, value[1], value[0], j)
                for key, value in pawn_row.items() for j in col])
        white_king = King('white', 'K', 1, 'e')
        black_king = King('black', 'k', 8, 'e')
        x = [white_king, Queen('white', 'Q', 1, 'd'),
             Rook('white', 'R', 1, 'a'), Rook('white', 'R', 1, 'h'),
             Knight('white', 'N', 1, 'b'), Knight('white', 'N', 1, 'g'),
             Bishop('white', 'B', 1, 'c'), Bishop('white', 'B', 1, 'f'),
             black_king, Queen('black', 'q', 8, 'd'),
             Rook('black', 'r', 8, 'a'), Rook('black', 'r', 8, 'h'),
             Knight('black', 'n', 8, 'b'), Knight('black', 'n', 8, 'g'),
             Bishop('black', 'b', 8, 'c'), Bishop('black', 'b', 8, 'f')] + pawn
        self.placeholder = {(i.row, i.column): i for i in x}
        self.lastmoves.append((x[0], 1, 'e', '  '))
        return self.placeholder, white_king, black_king

    def update_place_holder(self, piece):
        """Update new position."""
        prom = {'1': (Queen, 'Queen', 'Q'), '2': (Knight, 'Knight', 'N'),
                '3': (Rook, 'Rook', 'R'), '4': (Bishop, 'Bishop', 'B')}
        # place holder updating for normal move
        row, column, rowi, coli = piece.get_position()
        capture = self.placeholder.get((row, column), '  ')
        self.placeholder[(row, column)] = piece
        self.placeholder[(rowi, coli)] = '  '

        # place holder updating for prawn promotion
        if piece.name in ['P', 'p'] and row in [1, 8]:
            print('Promotion to')
            for key, value in prom.items():
                print(key, value[1])
            opt = input('choose your option: ')
            piece_c = prom[opt][0]
            name_c = prom[opt][2]
            name_c = name_c if piece.color is 'white' else name_c.lower()
            self.placeholder[(row, column)] = (piece_c('white', name_c,
                                                       row, column))

        col = ord(column)
        c_i = ord(coli)
        # place holder update for castling
        diff = abs(col - c_i)
        if piece.name in ['K', 'k'] and diff == 2:
            x = col+1 if col > c_i else col-2
            rook = self.placeholder.get((row, chr(x)), '  ')
            rp, cp, ri, ci = rook.get_position()
            is_check = piece.is_check(self)
            if not is_check:
                c_c = col - 1 if col > c_i else col + 1
                self.placeholder[(row, chr(c_c))] = (rook
                                                     .update_position(
                                                              row, chr(c_c)))
                self.placeholder[(row, cp)] = '  '

        # place holder update for en passant
        if all([piece.name in ['p', 'P'], abs(row-rowi) == 1,
                abs(col - c_i) == 1, capture == '  ']):
            r_c = row - 1 if piece.name is 'P' else row + 1
            capture = self.placeholder[(r_c, column)]
            self.placeholder[(r_c, column)] = '  '
        self.lastmoves.append((piece, row, column, capture, rowi, coli))

    def get_piece(self, row, column):
        """Return peace at that position."""
        return self.placeholder.get((row, column), '  ')

    def undo(self):
        """Undo one step."""
        piece, row, column, capture, rowi, coli = self.lastmoves[-1]
        rf, cf, ri, ci = piece.get_position()
        # undo last move
        if capture != '  ':
            # undo for en passant
            self.placeholder[(capture.row, capture.column)] = capture
            self.placeholder[(row, column)] = '  '
        else:
            self.placeholder[(row, column)] = capture
        piece.row = ri
        piece.column = ci
        piece.row_init = rowi
        piece.col_init = coli
        self.placeholder[(ri, ci)] = piece
        self.lastmoves = self.lastmoves[:-1]
        # undo for castling
        if ord(cf) - ord(ci) == 2 and piece.name in ['K', 'k']:
            x = ord(cf)+1 if ord(cf) > ord(ci) else ord(cf)-2
            x1 = ord(cf)-1 if ord(cf) > ord(ci) else ord(cf)+1
            name = 'R' if piece.name == 'K' else 'r'
            self.placeholder[(rf, chr(x))] = Rook(piece.color, name,
                                                  rf, chr(x))
            self.placeholder[(rf, chr(x1))] = '  '

    def print_board(self):
        """Print board."""
        print('{:^33}'.format('++++black++++')+'\n  ', end='')
        for i in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
            print('{:^4}'.format(i), end='')
        print('\n '+'-'*33)
        for row in range(8, 0, -1):
            print(f'{row}|', end='')
            for column in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
                x = self.placeholder.get((row, column), '  ')
                if x != '  ':
                    x = x.name
                t = '{:^3}'.format(x)
                print(t, end='|')
            print(f'{row}\n |'+'---|'*8)
        print('  ', end='')
        for i in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
            print('{:^4}'.format(i), end='')
        print('\n'+'{:^33}'.format('++++white++++'))


class King(Piece):
    """Implement king."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check current move is valid or not."""
        r_p, c_p, r_i, c_i = self.get_position()
        c_p, col = ord(c_p), ord(col)
        rule = row in [r_p-1, r_p, r_p+1] and col in [c_p-1, c_p, c_p+1]

        x = col+1 if col > c_p else col-2
        is_piece = ph.placeholder.get((row, chr(x)), '  ')

        # castling
        rule4 = False
        # checking is king moved or not? and
        # is rook piece present at a or h postions
        if all([row == r_p, r_p == r_i, chr(c_p) == c_i, is_piece != '  ']):
            rule1 = is_piece.name in ['R', 'r']
            rp, cp, ri, ci = is_piece.get_position()
            rule2 = rp == ri and cp == ci
            rule3 = all([rule1, rule2])
            i = 1 if self.color is 'white' else 8
            # is rook moved from initial position or not?
            if rule3 and col > c_p:
                rule4 = (all(map(lambda x: ph.get_piece(i, chr(c_p+x)) == '  ',
                                 range(ord(cp)+1, ord('h')))))
            elif rule3 and col < c_p:
                rule4 = (all(map(lambda x: ph.get_piece(i, chr(c_p-x)) == '  ',
                                 range(ord('a')-1, ord(cp)))))
        return rule or rule4

    def is_check(self, ph):
        """Is king in check position."""
        r_p, c_p, r_i, c_i = self.get_position()
        for key, value in ph.placeholder.items():
            if value != '  ':
                if value.color != self.color and value.is_can(r_p, c_p, ph):
                    return True
        return False


class Queen(Piece):
    """Queen class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check move is valide or not."""
        r_p, c_p, r_i, c_i = self.get_position()
        rule1 = Rook.is_can(self, row, col, ph)
        rule2 = Bishop.is_can(self, row, col, ph)
        return rule1 or rule2


class Rook(Piece):
    """Rook class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check present move is right or not."""
        rule1 = False
        r_p, c_p, r_i, c_i = self.get_position()
        c_p, col = ord(c_p), ord(col)
        if r_p < row and c_p == col:
            rule1 = all(map(lambda x: ph.get_piece(x, chr(c_p)) == '  ',
                            range(r_p+1, row)))
        elif r_p > row and c_p == col:
            rule1 = all(map(lambda x: ph.get_piece(x, chr(c_p)) == '  ',
                            range(row+1, r_p)))
        elif c_p < col and row == r_p:
            rule1 = all(map(lambda x: ph.get_piece(r_p, chr(x)) == '  ',
                            range(c_p+1, col)))
        elif c_p > col and row == r_p:
            rule1 = all(map(lambda x: ph.get_piece(r_p, chr(x)) == '  ',
                            range(col+1, c_p)))

        return rule1


class Knight(Piece):
    """Knight class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check move is valide or not."""
        r_p, c_p, r_i, c_i = self.get_position()
        c_p = ord(c_p)
        col = ord(col)
        chances = [(r_p+2, c_p+1), (r_p+2, c_p-1), (r_p-2, c_p+1),
                   (r_p-2, c_p-1), (r_p+1, c_p+2), (r_p+1, c_p-2),
                   (r_p-1, c_p+2), (r_p-1, c_p-2)]
        return (row, col) in chances


class Bishop(Piece):
    """Bishop class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check move is valide or not."""
        rule1 = False
        r_p, c_p, r_i, c_i = self.get_position()
        c_p, col = ord(c_p), ord(col)
        if abs(r_p-row) == abs(c_p-col):
            if r_p < row and c_p < col:
                rule1 = (all(map(
                        lambda x: ph.get_piece(r_p+x, chr(c_p+x)) == '  ',
                        range(1, row-r_p))))
            elif r_p < row and c_p > col:
                rule1 = (all(map(
                        lambda x: ph.get_piece(r_p+x, chr(c_p-x)) == '  ',
                        range(1, row-r_p))))
            elif r_p > row and c_p < col:
                rule1 = (all(map(
                        lambda x: ph.get_piece(r_p-x, chr(c_p+x)) == '  ',
                        range(1, r_p-row))))
            elif r_p > row and c_p > col:
                rule1 = (all(map(
                        lambda x: ph.get_piece(r_p-x, chr(c_p-x)) == '  ',
                        range(1, r_p-row))))
        return rule1


class Pawn(Piece):
    """Pawn class."""

    def __init__(self, color, name, row, col):
        """Construct for king."""
        Piece.__init__(self, color, name, row, col)

    def is_can(self, row, col, ph):
        """Check present move is right or not."""
        r_p, c_p, r_i, c_i = self.get_position()
        piece = ph.get_piece(row, col)
        rule3 = ph.get_piece(row, col) == '  '
        rule4 = False
        rule5 = False

        # condition for one jump and two jumps
        if self.color is 'white':
            rule1 = r_p is 2 and r_p+2 is row and c_p is col
            rule2 = r_p+1 is row and c_p is col
        else:
            rule1 = r_p is 7 and r_p-2 is row and c_p is col
            rule2 = r_p-1 is row and c_p is col

        # condition for cross jump
        if piece != '  ' and self.color is 'white':
            rule4 = ord(col) in [ord(c_p)+1, ord(c_p)-1] and r_p+1 is row
        elif piece != '  ' and self.color is 'black':
            rule4 = ord(col) in [ord(c_p)+1, ord(c_p)-1] and r_p-1 is row

        # en passant condition
        last_move = ph.lastmoves[-1]
        diff = abs(last_move[0].row - last_move[0].row_init)
        rule5 = all([last_move[0].name in ['p', 'P'],
                     diff == 2, rule3, last_move[2] == col])
        return any([(rule1 or rule2) and rule3, rule4, rule5])


def check_input(ph, color, wking, bking):
    """Check input."""
    col = list('abcdefgh')
    row = [1, 2, 3, 4, 5, 6, 7, 8]
    inp = input('Make a move: ')
    pieces = ph.placeholder
    if len(inp) == 4 and inp[1].isdigit() and inp[3].isdigit():
        c_i, r_i, c_f, r_f = inp[0], int(inp[1]), inp[2], int(inp[3])
        inp = all([c_i in col, r_i in row, c_f in col, r_f in row])
        if inp and pieces.get((r_i, c_i), '  ') != '  ':
            return r_i, c_i, r_f, c_f
        else:
            print('Invalid Input')
    else:
        print('Invalid Inputs')
    return check_input(ph, color, wking, bking)


def main():
    """Main."""
    piece_places = PlaceHolder()
    _, white_king, black_king = piece_places.initial_placehold()
    piece_places.print_board()
    curr_move = 'white'
    while True:
        move_kill_king = False
        print(f'{curr_move} move .....')
        row_i, col_i, row_f, col_f = check_input(piece_places, curr_move,
                                                 white_king, black_king)
        move_piece = piece_places.get_piece(row_i, col_i)
        piece = piece_places.get_piece(row_f, col_f)
        can_not_move = not move_piece.is_can(row_f, col_f, piece_places)
        color_match = False
        if piece != '  ':
            color_match = piece.color == move_piece.color
            move_kill_king = piece.name in ['K', 'k']
        if any([curr_move != move_piece.color, can_not_move, color_match]):
            print('Invalid Move')
            continue
        move_piece.update_position(row_f, col_f)
        piece_places.update_place_holder(move_piece)
        curr_king = white_king if curr_move is 'white' else black_king
        if curr_king.is_check(piece_places):
            piece_places.undo()
            print('King is checked or move make king check')
            continue
        piece_places.print_board()
        print('Press 1 : Undo, 2 : Resign, 3: Exit  press any key to continue')
        choice = input('Enter your choice: ')
        if choice == '1':
            piece_places.undo()
            piece_places.print_board()
            continue
        elif choice == '2':
            win = 'black' if curr_move == 'white' else 'white'
            print(f"{win} won the game")
            break
        elif choice == '3':
            print('Exit game')
            break
        elif move_kill_king:
            print(f'{curr_move} won the game')
            break
        curr_move = 'black' if curr_move == 'white' else 'white'


main()
